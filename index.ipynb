{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Smart Grid Research\n",
    "## \"Hands-on\" Simulation with mosaik\n",
    "\n",
    "### Introduction\n",
    "#### Background\n",
    "Smart grids represent an evolution in electricity network management, integrating renewable energy sources, improving efficiency, and increasing the reliability of electricity distribution. Simulations play a critical role in the design, analysis, and optimization of smart grid technologies, enabling researchers and engineers to test scenarios that would be impractical or impossible to perform in real life.\n",
    "\n",
    "#### Objective\n",
    "In this notebook, we demonstrate a simulation of a smart grid ecosystem using the mosaik simulation framework. We'll simulate various components including wind turbines, power plants, batteries, and photovoltaic (PV) systems, showcasing how these elements interact within a smart grid."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Setting up the scenario\n",
    "To get started, import the necessary libraries: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.207248241Z",
     "start_time": "2024-03-22T08:52:21.019991992Z"
    }
   },
   "outputs": [],
   "source": [
    "import import_ipynb\n",
    "\n",
    "# Only for Jupyter notebooks\n",
    "import nest_asyncio\n",
    "\n",
    "nest_asyncio.apply()\n",
    "\n",
    "import mosaik.util"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulation Configuration\n",
    "In this section, we configure the simulators for our smart grid components. Each simulator represents a different aspect of the smart grid, from renewable energy sources to storage solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.302005313Z",
     "start_time": "2024-03-22T08:52:21.036164689Z"
    }
   },
   "outputs": [],
   "source": [
    "SIM_CONFIG = {\n",
    "    'CSV': {\n",
    "        'python': 'mosaik_csv:CSV',\n",
    "    },\n",
    "    'Wind': {\n",
    "        'python': 'mosaik_components.wind:Simulator'\n",
    "    },\n",
    "    'PowerPlant': {\n",
    "        'python': 'mosaik_components.powerplant:Simulator'\n",
    "    },\n",
    "    'Battery': {\n",
    "        'python': 'mosaik_components.battery:Simulator'\n",
    "    },\n",
    "    'PV': {\n",
    "        'python': 'mosaik_components.pv.pvsimulator:PVSimulator'\n",
    "    },\n",
    "    'Collector': {\n",
    "        'python': 'simulators.collector:Collector'\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to define a mosaik World object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.318635658Z",
     "start_time": "2024-03-22T08:52:21.043171706Z"
    }
   },
   "outputs": [],
   "source": [
    "world = mosaik.World(SIM_CONFIG)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simulation duration is defined to be 5 minutes in milliseconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.325629571Z",
     "start_time": "2024-03-22T08:52:21.052621131Z"
    }
   },
   "outputs": [],
   "source": [
    "END = 5 * 60 * 1000 + 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Component Simulations\n",
    "#### Wind Simulation\n",
    "We simulate a wind turbine using wind speed data and a power coefficient curve. The simulation demonstrates how varying wind speeds affect power generation.\n",
    "\n",
    "*Data Inputs*\n",
    "Wind speed data (wind_data_bhv.csv) represents the varying speeds over time.\n",
    "Power coefficient curve (power_coeff_curve_arek.csv) relates wind speed to power output.\n",
    "Simulator Configuration\n",
    "We initialize the wind simulator with the CSV file paths and configure the simulation start time and step size."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.329086993Z",
     "start_time": "2024-03-22T08:52:21.062287067Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-03-22 09:52:21.055\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"CSV\" as \"CSV-0\" ...\u001b[0m\n",
      "\u001b[32m2024-03-22 09:52:21.058\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"Wind\" as \"Wind-0\" ...\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "# WIND\n",
    "WIND_DATA = 'csv_data/wind_data_bhv.csv'\n",
    "POWER_CURVE_DATA = 'csv_data/power_coeff_curve_arek.csv'\n",
    "SIM_START_WIND = '2021-02-24 00:00:00'\n",
    "\n",
    "windData = world.start(\"CSV\",\n",
    "                       sim_start=SIM_START_WIND,\n",
    "                       datafile=WIND_DATA)\n",
    "\n",
    "wind_simulator = world.start('Wind',\n",
    "                             power_curve_csv=POWER_CURVE_DATA,\n",
    "                             step_size=60000,\n",
    "                             gen_neg=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Power Plant Simulation\n",
    "Simulates both natural gas and waste power plants, demonstrating how different types of power plants can be modeled and integrated into the smart grid."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.335189370Z",
     "start_time": "2024-03-22T08:52:21.071844344Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-03-22 09:52:21.066\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"CSV\" as \"CSV-1\" ...\u001b[0m\n",
      "\u001b[32m2024-03-22 09:52:21.069\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"CSV\" as \"CSV-2\" ...\u001b[0m\n",
      "\u001b[32m2024-03-22 09:52:21.070\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"PowerPlant\" as \"PowerPlant-0\" ...\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "# POWERPLANT\n",
    "POWERPLANT_NATURALGAS_DATA = 'csv_data/powerplant_NaturalGas.csv'\n",
    "SIM_START_NATURALGAS_PP = '2018-01-01 00:00:00'\n",
    "POWERPLANT_WASTE_DATA = 'csv_data/powerplant_Waste.csv'\n",
    "SIM_START_WASTE_PP = '2018-01-01 00:00:00'\n",
    "\n",
    "powerplant_NaturalGas_Data = world.start(\"CSV\",\n",
    "                                         sim_start=SIM_START_NATURALGAS_PP,\n",
    "                                         datafile=POWERPLANT_NATURALGAS_DATA)\n",
    "\n",
    "powerplant_Waste_Data = world.start(\"CSV\",\n",
    "                                    sim_start=SIM_START_WASTE_PP,\n",
    "                                    datafile=POWERPLANT_WASTE_DATA)\n",
    "\n",
    "powerplant_simulator = world.start('PowerPlant',\n",
    "                                   step_size=60000,\n",
    "                                   number_of_schedules=5,\n",
    "                                   ramp_behavior=[2, 2],\n",
    "                                   min_power=0,\n",
    "                                   max_power=14,\n",
    "                                   interval_size=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Battery Storage\n",
    "Represents a battery storage system, showcasing how energy can be stored during periods of excess generation and released during demand peaks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.338414585Z",
     "start_time": "2024-03-22T08:52:21.088120332Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-03-22 09:52:21.077\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"Battery\" as \"Battery-0\" ...\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "# BATTERY\n",
    "battery_simulator = world.start('Battery',\n",
    "                                step_size=60000,\n",
    "                                number_of_schedules=5,\n",
    "                                charge_power=11.0,\n",
    "                                discharge_power=150.0,\n",
    "                                battery_capacity=80,\n",
    "                                start_soc=0.1,\n",
    "                                interval_size=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### PV Simulation\n",
    "Simulates a photovoltaic system, showing how solar power can be integrated into the smart grid, varying with sunlight availability.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.342084845Z",
     "start_time": "2024-03-22T08:52:21.088434840Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-03-22 09:52:21.085\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"CSV\" as \"CSV-3\" ...\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "# PV\n",
    "PV_DATA = 'csv_data/pv_10kw.csv'\n",
    "SIM_START = '2014-01-01 00:00:00'\n",
    "\n",
    "pv_Data = world.start(\"CSV\",\n",
    "                      sim_start=SIM_START,\n",
    "                      datafile=PV_DATA)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Collector\n",
    "Collects and monitors data from all components, crucial for analyzing the overall performance of the smart grid simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.349980947Z",
     "start_time": "2024-03-22T08:52:21.110283422Z"
    }
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-03-22 09:52:21.092\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mmosaik.scenario\u001b[0m:\u001b[36mstart\u001b[0m:\u001b[36m280\u001b[0m - \u001b[1mStarting \"Collector\" as \"Collector-0\" ...\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "# COLLECTOR\n",
    "collector = world.start('Collector')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Instantiation\n",
    "Now we need to instantiate the different simulation models. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.423197856Z",
     "start_time": "2024-03-22T08:52:21.110668597Z"
    }
   },
   "outputs": [],
   "source": [
    "# INSTANTIATION\n",
    "wind_speed = windData.Wind()\n",
    "wind_sim_model = wind_simulator.WT()\n",
    "\n",
    "powerplant_naturalgas_data = powerplant_NaturalGas_Data.NaturalGas()\n",
    "powerplant_waste_data = powerplant_Waste_Data.Waste()\n",
    "powerplant_sim_model = powerplant_simulator.Powerplant()\n",
    "\n",
    "battery_sim_model = battery_simulator.Battery()\n",
    "\n",
    "pv_data = pv_Data.PV()\n",
    "\n",
    "monitor = collector.Monitor()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connections\n",
    "In mosaik, we need to define connections between simulation models in order to define data flows. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:21.423687743Z",
     "start_time": "2024-03-22T08:52:21.200807520Z"
    }
   },
   "outputs": [],
   "source": [
    "# CONNECTIONS\n",
    "world.connect(wind_speed, wind_sim_model, 'wind_speed')\n",
    "world.connect(wind_sim_model, monitor, 'P_gen')\n",
    "\n",
    "world.connect(powerplant_naturalgas_data, powerplant_sim_model, 'p_mw')\n",
    "world.connect(powerplant_waste_data, powerplant_sim_model, 'p_mw')\n",
    "world.connect(powerplant_sim_model, monitor, 'schedules')\n",
    "\n",
    "world.connect(powerplant_sim_model, battery_sim_model, 'schedules')\n",
    "world.connect(battery_sim_model, monitor, 'P_gen', 'schedules')\n",
    "\n",
    "world.connect(pv_data, monitor, 'P[MW]')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Execute the simulation\n",
    "Finally, we execute the simulation. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "world.run(until=END)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***\n",
    "## Questions/Excercises\n",
    "1. Analyze the dependencies between the simulation models and draw an overview that shows the simulation models and the input/output data.\n",
    "2. What does the collected data contain? \n",
    "3. Add some visualization of the simulation data by modification of the collector simulator.\n",
    "4. Modify the wind speed data and observe how it affects power generation.\n",
    "5. Experiment with different battery capacities and analyze the impact.\n",
    "6. Add a controller simulator that controls the battery and power plants to balance supply and demand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {
    "ExecuteTime": {
     "end_time": "2024-03-22T08:52:26.565918117Z",
     "start_time": "2024-03-22T08:52:26.563397088Z"
    }
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
