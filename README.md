# Smart Grid Research 
## "Hands-on" Simulations with mosaik

Launch this notebook on binder by following this link:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/malin_radtke%2Fsmart-grid-research/main)
